#![no_std]

use embassy_rp::i2c::{self, Blocking, Config as I2CConfig, I2c};
use embassy_rp::peripherals::I2C1;
use embassy_rp::{Peripheral, Peripherals};
use embassy_time::{Duration, Timer};

use eeprom24x::{addr_size, page_size, Eeprom24x, SlaveAddr};

pub type Ee24x08 = Eeprom24x<
    I2c<'static, I2C1, Blocking>,
    page_size::B16,
    addr_size::OneByte,
    eeprom24x::unique_serial::No,
>;

// Memory usage of the Eeprom 24x08
#[allow(non_snake_case)]
pub const PAGE_SIZE: u32 = 16;

#[allow(non_camel_case_types)]
pub enum ADDR_SIZE {
    OneByte,
    TwoBytes,
}

pub fn create_24x08_eeprom(p: &mut Peripherals) -> Ee24x08 {
    let sda = unsafe { p.PIN_14.clone_unchecked() };
    let scl = unsafe { p.PIN_15.clone_unchecked() };
    let mut i2c_config = I2CConfig::default();
    i2c_config.frequency = 1_000_000;
    let i2c_channel = unsafe { p.I2C1.clone_unchecked() };
    let i2c = i2c::I2c::new_blocking(i2c_channel, scl, sda, i2c_config);

    let eeprom = Eeprom24x::new_24x08(i2c, SlaveAddr::default());

    eeprom
}

fn u8_to_byte_array(value: u8) -> [u8; 1] {
    let b = (value & 0xFF) as u8;
    [b]
}

fn u16_to_byte_array(value: u16) -> [u8; 2] {
    let b1 = ((value >> 8) & 0xFF) as u8;
    let b2 = (value & 0xFF) as u8;
    [b1, b2]
}

pub async fn write_buf(eeprom: &mut Ee24x08, buf: &[u8], addr: u32) {
    let mut ee_mem_addr = addr;

    for data_chunk in buf[..buf.len()].chunks(PAGE_SIZE as usize) {
        let _ = eeprom.write_page(ee_mem_addr, &data_chunk); // WARN Result ignored
        ee_mem_addr += PAGE_SIZE;
        let delay = Duration::from_millis(5);
        Timer::after(delay).await;
    }
}

pub async fn write_buf_len(eeprom: &mut Ee24x08, addr: u32, len: u32, ee_addr_size: ADDR_SIZE) {
    match ee_addr_size {
        ADDR_SIZE::OneByte => {
            let len = u8_to_byte_array(len as u8); // Panic if > 8bit
            let _ = eeprom.write_byte(addr, len[0]);
            let delay = Duration::from_millis(5);
            Timer::after(delay).await;
        }
        ADDR_SIZE::TwoBytes => {
            // Memorise size on last 2 eerpom byte
            let len = u16_to_byte_array(len as u16); // Panic if > 16bit
            let _ = eeprom.write_byte(addr, len[0]);
            let delay = Duration::from_millis(5);
            Timer::after(delay).await;
            let _ = eeprom.write_byte(addr + 1, len[1]);
            let delay = Duration::from_millis(5);
            Timer::after(delay).await;
        }
    }
}

pub async fn read_buf(eeprom: &mut Ee24x08, buf: &mut [u8], addr: u32) {
    let _ = eeprom.read_data(addr, buf);
}

pub async fn read_buf_len(eeprom: &mut Ee24x08, addr: u32, ee_addr_size: ADDR_SIZE) -> usize {
    match ee_addr_size {
        ADDR_SIZE::OneByte => {
            let val = eeprom.read_byte(addr).unwrap();
            return val as usize;
        }
        ADDR_SIZE::TwoBytes => {
            let byte1 = eeprom.read_byte(addr).unwrap();
            let byte2 = eeprom.read_byte(addr + 1).unwrap();
            return ((byte1 as usize) << 8) | (byte2 as usize);
        }
    };
}
